const EXTENSION_OWNER = 'http://localhost:8888';

console.log('tabs', chrome.tabs)
console.log('chrome', chrome)


chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    console.log('arguments in background script ', request);
    switch(request.type){
        case 'parse_html':
            console.log('try to parse');
            $.ajax({
                // type: "GET",
                type: "POST",
                // url: EXTENSION_OWNER,
                url: `${EXTENSION_OWNER}/parse_html`,
                data: request,
                dataType: 'json',
                cache: false,
                success: function(data){
                    console.log(request.type,' response: ', data);
                },
                error: function(xhr, type){
                    alert('ошибка при парсинге');
                    console.log(request.type,' ajax error', xhr, type);
                }
            })
            break;

        case 'send_cookie':
            console.log('некто попытался поделиться куки');
            break;

        case 'include_style':
            // console.log('попытка подключить стили');
            // console.log('по пути: ', `styles/${request.filename}`)
            chrome.tabs.insertCSS({
                file: `styles/${request.filename}`
            })
            break;

        default:
            console.warn('неизвестное действие');
    }

    // var xhr = new XMLHttpRequest();
    // xhr.onreadystatechange = function(){
    //     var DONE = 4; // readyState 4 means the request is done.
    //     var OK = 200; // status 200 is a successful return.
    //     if (xhr.readyState === DONE) {
    //         if (xhr.status === OK) {
    //             console.log(xhr.responseText); // 'This is the returned text.'
    //         } else {
    //             console.log('Error: ' + xhr.status); // An error occurred during the request.
    //         }
    //     }
    // }
    // // xhr.open("GET", chrome.extension.getURL('/config_resources/config.json'), true);
    // xhr.open("GET", 'http://localhost:8888');
    // xhr.send();
})
