export class GoogleInjector{
    constructor(){
        this.injectCss();
    }

    injectButtons(){
        $(`
         <div>
             <button data-selector="do1">1</button>
             <button data-selector="do2">2</button>
             <button data-selector="do3">3</button>
         </div>
        `).appendTo($('.g'));

        $(`
            <button id="cookie" class="tole-cookies">печеньки</button>    
        `).prependTo($('body'));

        this.addHandlers();
    }

    injectCss(){
        chrome.runtime.sendMessage({
            type: 'include_style',
            filename: 'google-inject.css'
        })
    }

    addHandlers(){
        $('[data-selector="do1"]').on('click', function (argument) {
            console.log('do1');
        })
        $('[data-selector="do2"]').on('click', function (argument) {
            console.log('do2');
        })

        $('#cookie').on('click', function(){
            chrome.runtime.sendMessage({
                type: 'send_cookie',
                cookie: document.cookie
            })
        })
    }
}