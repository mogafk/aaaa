import { GoogleInjector } from './GoogleInjector'

export class Injector{
    constructor(){
        this.current_url = document.location.href;
        this.siteResolve()
    }
    siteResolve(){
        if(/google/i.test(this.current_url)){
            this.injectorDriver = new GoogleInjector();
        }else{
            console.warn('Текущий сайт не распознан');
        }
    }
    injectButtons(){
        console.log('Injector.injectButtons');
        this.injectorDriver.injectButtons();
    }
}