var path = require('path');

module.exports = {
  entry: {
    filename: './google_extension_src/injected_script.js',
    // path: path.resolve(__dirname, 'google_extension_src')
  },
  output: {
    filename: 'injected_script.js',
    path: path.resolve(__dirname, 'google_extension')
  },
  module: {
    rules: [
        {
            test: /\.js$/,
            use: [{
              loader: 'babel-loader',
              options: { 
                presets: ['es2015'],
               },
            }],
        },
    ]
  }
};